Thorough = {
	
	Resources = {
		Wealth = {
			NAME "Wealth"
			SUMMARY "Represent the economical capacity of our empire."
		}
		PoliticalPower = {
			NAME "Political Power"
			SUMMARY "Represents the capacity for reform; used to add, remove and improve levers."
		}
	}
	Eras = {
		MezoStoneAge = {
			NAME "Mezo Stone Age"
			DESCRIPTION "Mezo Stone Age"
		}
		BronzeAge = {
			NAME "Bronze Age"
			DESCRIPTION "Bronze Age"
		}		
	}
	
	Dropdowns = {
        }
	
	Windows = {
	}
	
	Alerts = {		
	}	
	
	Nodes = {
		States = {
				Population = {
					NAME "Population"
					DESCRIPTION "One point of population is 1000 people"
				}

				Food = {
					NAME "Food"
					DESCRIPTION "Food. One point of food is 1000 suplise on year for one human"
				}
				Demography = {
					NAME "Demography"
					DESCRIPTION "Demography"
				}
			}
		Levers = {
				Fishing = {
					NAME "Fishing"
					DESCRIPTION "How many people work in fishing"
				}

				Hunting = {
					NAME "Hunting"
					DESCRIPTION "How many people work in hunting"
				}
				Foraging = {
					NAME "Foraging"
					DESCRIPTION "How many people work in foraging"
				}


			}
		Events = {
}
}
}
